resource "aws_vpc" "dulce-vpc-tf" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
      Name = "dulce-vpc-tf"
  }
}

resource "aws_subnet" "public-tf-dulce" {
  vpc_id = aws_vpc.dulce-vpc-tf.id
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
  availability_zone = "us-east-2a"

  tags = {
      Name = "public-tf-dulce"
  }
}

resource "aws_subnet" "private-tf-dulce" {
  vpc_id = aws_vpc.dulce-vpc-tf.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "us-east-2b"

  tags = {
      Name = "private-tf-dulce"
  }
}

resource "aws_route_table" "public-rt-dulce" {
  vpc_id = aws_vpc.dulce-vpc-tf.id

  tags = {
      Name = "public-rt-dulce"
  }
}

resource "aws_internet_gateway" "igw-dulce-tf" {
  vpc_id = aws_vpc.dulce-vpc-tf.id

  tags = {
      Name = "igw-dulce-tf"
  }
}

resource "aws_route_table_association" "public-tf-dulce" {
  subnet_id = aws_subnet.public-tf-dulce.id
  route_table_id = aws_route_table.public-rt-dulce.id
}

resource "aws_route_table_association" "private-tf-dulce" {
  subnet_id = aws_subnet.private-tf-dulce.id
  route_table_id = aws_route_table.public-rt-dulce.id
}

resource "aws_route" "public-route" {
  route_table_id = aws_route_table.public-rt-dulce.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.igw-dulce-tf.id
}

resource "aws_security_group" "dulce-sg-tf" {
    name = "dulce-sg-tf"
    vpc_id = aws_vpc.dulce-vpc-tf.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "dulce_key_tf" {
  key_name   = "dulce.key"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "aws_ami" "ami-dulce" {
    most_recent = true

    filter {
        name   = "name"
        values = ["amzn2-ami-ecs-hvm-2.0.20190603-x86_64-ebs"]
    }
    owners = ["591542846629"]
}

resource "aws_instance" "instance-web" {
  ami = data.aws_ami.ami-dulce.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.public-tf-dulce.id
  vpc_security_group_ids = [aws_security_group.dulce-sg-tf.id]
  key_name = aws_key_pair.dulce_key_tf.key_name

  tags = {
    Name = "Dulce-instance-tf"
  }
}

output "instance_ip4"{
    value = aws_instance.instance-web.public_ip
}